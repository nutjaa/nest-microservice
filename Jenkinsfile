pipeline {
    agent none
    environment {
		DOCKERHUB_CREDENTIALS=credentials('dockerhub-cred-nutjaa')
	}
    options {
        disableConcurrentBuilds()
    }
    stages {
        stage('Prepare') {
            agent {
                docker {
                    image 'node:lts-bullseye-slim'
                    args '-u root:root'
                }
            }
            steps {
                echo 'Start prepare environment'
                sh 'node --version'
                sh 'npm install'
                updateGitlabCommitStatus name: 'prepare', state: 'success'
            }
        }
        stage('Test') {
            agent {
                docker {
                    image 'node:lts-bullseye-slim'
                    args '-u root:root'
                }
            }
            steps {
                echo 'Start testing'
                sh 'cp ./apps/main/.env.testing ./apps/main/.env'
                sh 'cp ./apps/auth/.env.testing ./apps/auth/.env'
                sh 'cp ./apps/media/.env.testing ./apps/media/.env'
                sh 'npm run test'
                updateGitlabCommitStatus name: 'test', state: 'success'
            }
        }

        stage('Build - integration') {
            agent any
            when {
                expression {
                    env.GIT_BRANCH  == 'origin/master'
                }
            }
            steps {
                echo 'Start integration build and deployment 1'
                dir('./apps/main') {
                    sh "./docker-build.sh"
                }
                dir('./apps/auth') {
                    sh "./docker-build.sh"
                }
                dir('./apps/media') {
                    sh "./docker-build.sh"
                }
                updateGitlabCommitStatus name: 'build-integration', state: 'success'
            }
        }
        /*
        stage('Login - integration') {
            agent any
            when {
                expression {
                    env.GIT_BRANCH  == 'origin/master'
                }
            }
			steps {
				sh 'echo $DOCKERHUB_CREDENTIALS_PSW | docker login -u $DOCKERHUB_CREDENTIALS_USR --password-stdin'
			}
		}
        */
		stage('Push ') {
            agent any
            when {
                expression {
                    env.GIT_BRANCH  == 'origin/master'
                }
            }
			steps {
				dir('./apps/main') {
                    sh "./docker-deploy.sh"
                }
                dir('./apps/auth') {
                    sh "./docker-deploy.sh"
                }
                dir('./apps/media') {
                    sh "./docker-deploy.sh"
                }
			}
		}

        stage('Deploy - manual integration') {
            agent any
            steps {
                sshagent(credentials: ['ssh-temp-ecs-integration']) {
                    sh 'ssh -o StrictHostKeyChecking=no -l root 203.154.91.6 uname -a'
                    sh 'ssh root@203.154.91.6 -t "cd work/test/ && docker-compose pull  && docker-compose up -d"'
                    // sh 'ssh root@203.154.91.6 -t "docker rmi $(docker images -qa -f \'dangling=true\')"'
                }
            }
        }
    }
}