import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsString } from 'class-validator';

export class RefreshTokenRequest {
  @ApiProperty({
    example: 'xxxxxxx',
  })
  @IsString()
  @IsNotEmpty()
  refresh_token: string;
}
