import { ApiProperty } from '@nestjs/swagger';
import { UserResponse } from './user.response';

export class UserJsonResponse {
  @ApiProperty()
  user: UserResponse;
}
