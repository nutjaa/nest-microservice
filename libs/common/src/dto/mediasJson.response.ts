import { ApiProperty } from '@nestjs/swagger';
import { MediaResponse } from './media.response';

export class MediasJsonResponse {
  @ApiProperty()
  medias: MediaResponse[];

  @ApiProperty()
  total: number[];
}
