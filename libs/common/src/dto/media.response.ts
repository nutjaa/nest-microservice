import { ApiProperty } from '@nestjs/swagger';

export class MediaResponse {
  @ApiProperty()
  _id: string;

  @ApiProperty()
  mediaType: number;

  @ApiProperty()
  key: string;

  @ApiProperty()
  createdAt: Date;

  @ApiProperty()
  updatedAt: Date;

  @ApiProperty()
  fullUrl: string;

  constructor(data: any) {
    this._id = data._id;
    this.mediaType = data.mediaType;
    this.createdAt = data.createdAt;
    this.updatedAt = data.updatedAt;
    this.key = data.key;
    console.log(`data.fullUrl ${data.fullUrl}`);
    this.fullUrl = data.fullUrl;
  }
}
