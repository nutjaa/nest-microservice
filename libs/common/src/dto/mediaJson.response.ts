import { ApiProperty } from '@nestjs/swagger';
import { MediaResponse } from './media.response';

export class MediaJsonResponse {
  @ApiProperty()
  media: MediaResponse;
}
