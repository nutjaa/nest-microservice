# Jenkins setup

## How to

1. docker compose up -d

### set nginx reverse proxy

1. set up nginx

```
sudo apt update
sudo apt install nginx
```

2. set up new site available
   nano /etc/nginx/sites-available/jenkins.peglowny.com

```
server {
    server_name jenkins.peglowny.com;

    location / {
        include /etc/nginx/proxy_params;
        proxy_pass          http://localhost:8080;
        proxy_read_timeout  90s;
        # Fix potential "It appears that your reverse proxy setup is broken" error.
        proxy_redirect      http://localhost:8080 https://jenkins.peglowny.com;
    }
}
```

3. test nginx config script
   sudo nginx -t

4. link to site enabled
   sudo ln -s /etc/nginx/sites-available/jenkins.peglowny.com /etc/nginx/sites-enabled/

5. restart nginx
   sudo systemctl restart nginx

6. install cert bot
   sudo snap install core; sudo snap refresh core
   sudo snap install --classic certbot

7. link cert bot command
   sudo ln -s /snap/bin/certbot /usr/bin/certbot

8. obtain an ssl certificate
   sudo certbot --nginx -d jenkins.peglowny.com

9. Verifying Certbot Auto-Renewal
   sudo systemctl status snap.certbot.renew.service

10. test the renewal process
    sudo certbot renew --dry-run
