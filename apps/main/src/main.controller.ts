import { Controller, Get } from '@nestjs/common';
import { MainService } from './main.service';

@Controller()
export class MainController {
  constructor(private readonly mainService: MainService) {}

  @Get()
  getHello(): string {
    return this.mainService.getHello();
  }

  @Get('test-auth')
  async getTestAuth(): Promise<string> {
    return await this.mainService.getHelloAuth();
  }

  @Get('test-media')
  async getTestMedia(): Promise<string> {
    return await this.mainService.getHelloMedia();
  }
}
