import {
  CallHandler,
  ExecutionContext,
  Injectable,
  NestInterceptor,
} from '@nestjs/common';
import { Observable } from 'rxjs';

/**
 * Injects request data into the context, so that the ValidationPipe can use it.
 */
@Injectable()
export class ContextInterceptor implements NestInterceptor {
  intercept(context: ExecutionContext, next: CallHandler): Observable<any> {
    const request = context.switchToHttp().getRequest();
    request.authentication = this.getAuthentication(context);
    return next.handle();
  }

  private getAuthentication(context: ExecutionContext) {
    let authentication: string;
    if (context.getType() === 'rpc') {
      authentication = context.switchToRpc().getData().authentication;
    } else if (context.getType() === 'http') {
      if (context.switchToHttp().getRequest()['headers'].authorization) {
        authentication = context
          .switchToHttp()
          .getRequest()
          ['headers'].authorization.replace('Bearer ', '');
      } else {
        authentication = context.switchToHttp().getRequest()
          .cookies?.Authentication;
      }
    }

    return authentication;
  }
}
