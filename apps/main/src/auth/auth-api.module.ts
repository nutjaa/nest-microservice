import { Module } from '@nestjs/common';
import { AuthApiController } from './auth-api.controller';
import { AuthApiService } from './auth-api.service';
import { RmqModule } from '@app/common';
import { AUTH_SERVICE } from '../constants/services';

@Module({
  imports: [RmqModule.register({ name: AUTH_SERVICE })],
  controllers: [AuthApiController],
  providers: [AuthApiService],
})
export class AuthApiModule {}
