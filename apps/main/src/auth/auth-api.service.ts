import { CreateUserRequest } from '@app/common/dto/auth/create-user.request';
import { LoginUserRequest } from '@app/common/dto/auth/login-user.request';
import { RefreshTokenRequest } from '@app/common/dto/auth/refresh-token.request';
import { UserResponse } from '@app/common/dto/user.response';
import {
  Inject,
  Injectable,
  Logger,
  UnprocessableEntityException,
} from '@nestjs/common';
import { ClientProxy } from '@nestjs/microservices';
import { lastValueFrom } from 'rxjs';
import { AUTH_SERVICE } from '../constants/services';

@Injectable()
export class AuthApiService {
  private readonly logger = new Logger(AuthApiService.name);

  constructor(@Inject(AUTH_SERVICE) private authClient: ClientProxy) {}

  async createUser(request: CreateUserRequest): Promise<UserResponse> {
    this.logger.log('create user');

    try {
      return await lastValueFrom(
        this.authClient.send<UserResponse>('user_create', {
          request,
        }),
      );
    } catch (error) {
      throw new UnprocessableEntityException('cannot create new user.');
    }
  }

  async loginUser(request: LoginUserRequest): Promise<[string, string]> {
    this.logger.log('auth login');
    return lastValueFrom<[string, string]>(
      this.authClient.send('auth_login', {
        request,
      }),
    );
  }

  async refreshToken(request: RefreshTokenRequest): Promise<[string, string]> {
    this.logger.log('refresh token');
    return lastValueFrom<[string, string]>(
      this.authClient.send('auth_refresh_token', {
        request,
      }),
    );
  }
}
