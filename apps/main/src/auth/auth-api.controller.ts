import { JwtAuthGuard } from '@app/common/auth/jwt-auth.guard';
import { CurrentUser } from '@app/common/decorator/current-user.decorator';
import { CreateUserRequest } from '@app/common/dto/auth/create-user.request';
import { LoginUserRequest } from '@app/common/dto/auth/login-user.request';
import { LoginUserResponse } from '@app/common/dto/auth/login-user.response';
import { RefreshTokenRequest } from '@app/common/dto/auth/refresh-token.request';
import { UserResponse } from '@app/common/dto/user.response';
import { UserJsonResponse } from '@app/common/dto/userJson.response';
import { Body, Controller, Get, Post, UseGuards } from '@nestjs/common';
import {
  ApiBearerAuth,
  ApiOkResponse,
  ApiOperation,
  ApiTags,
} from '@nestjs/swagger';
import { AuthApiService } from './auth-api.service';

@Controller('auth/users')
@ApiTags('auth')
export class AuthApiController {
  constructor(private readonly authApiService: AuthApiService) {}

  @Post()
  @ApiOperation({ summary: 'Create new user' })
  @ApiOkResponse({
    description: 'User response',
    type: UserJsonResponse,
  })
  async createUser(@Body() request: CreateUserRequest) {
    return {
      user: await this.authApiService.createUser(request),
    };
  }

  @Post('login')
  @ApiOkResponse({
    description: 'User login token',
    type: LoginUserResponse,
  })
  async login(@Body() request: LoginUserRequest): Promise<LoginUserResponse> {
    const res = await this.authApiService.loginUser(request);
    return {
      token: res[0],
      refresh_token: res[1],
    };
  }

  @Post('refresh_token')
  @ApiOkResponse({
    description: 'User login token',
    type: LoginUserResponse,
  })
  async refreshToken(
    @Body() request: RefreshTokenRequest,
  ): Promise<LoginUserResponse> {
    const res = await this.authApiService.refreshToken(request);
    return {
      token: res[0],
      refresh_token: res[1],
    };
  }

  @Get('me')
  @ApiBearerAuth()
  @ApiOkResponse({
    description: 'User response',
    type: UserJsonResponse,
  })
  @UseGuards(JwtAuthGuard)
  async me(@CurrentUser() user: UserResponse) {
    return { user: user };
  }
}
