import { Module } from '@nestjs/common';
import { MainController } from './main.controller';
import { MainService } from './main.service';
import { ConfigModule } from '@nestjs/config';
import * as Joi from 'joi';
import { DatabaseModule, RmqModule, AuthModule } from '@app/common';
import { MEDIA_SERVICE } from './constants/services';
import { AuthApiModule } from './auth/auth-api.module';
import { MediaApiModule } from './media/media-api.module';

@Module({
  imports: [
    ConfigModule.forRoot({
      isGlobal: true,
      validationSchema: Joi.object({
        MONGODB_URI: Joi.string().required(),
        PORT: Joi.number().required(),
      }),
      envFilePath: './apps/main/.env',
    }),
    DatabaseModule,
    RmqModule.register({
      name: MEDIA_SERVICE,
    }),
    AuthModule,
    AuthApiModule,
    MediaApiModule,
  ],
  controllers: [MainController],
  providers: [MainService],
})
export class MainModule {}
