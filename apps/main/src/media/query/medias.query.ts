import { IsNumber, IsOptional } from 'class-validator';
import { Transform } from 'class-transformer';
import { ApiProperty } from '@nestjs/swagger';

export class MediasQuery {
  @IsOptional()
  @IsNumber()
  @Transform((value) => parseInt(value.value))
  @ApiProperty({
    required: false,
    name: 'page',
    description: 'Page number',
  })
  page?: number;

  @IsOptional()
  @IsNumber()
  @Transform((value) => parseInt(value.value))
  @ApiProperty({
    required: false,
    name: 'limit',
    description: 'limit',
  })
  limit?: number;
}
