import { Module } from '@nestjs/common';
import { MediaApiController } from './media-api.controller';
import { MediaApiService } from './media-api.service';
import { AuthModule, RmqModule } from '@app/common';
import { MEDIA_SERVICE } from '../constants/services';

@Module({
  imports: [RmqModule.register({ name: MEDIA_SERVICE }), AuthModule],
  controllers: [MediaApiController],
  providers: [MediaApiService],
})
export class MediaApiModule {}
