import { JwtAuthGuard } from '@app/common/auth/jwt-auth.guard';
import { MediaJsonResponse } from '@app/common/dto/mediaJson.response';
import { MediasJsonResponse } from '@app/common/dto/mediasJson.response';
import {
  Controller,
  Get,
  Param,
  Post,
  Query,
  Req,
  UseGuards,
} from '@nestjs/common';
import {
  ApiBearerAuth,
  ApiOkResponse,
  ApiOperation,
  ApiResponse,
  ApiTags,
} from '@nestjs/swagger';
import { MediaApiService } from './media-api.service';
import { MediasQuery } from './query/medias.query';

@Controller('medias')
@ApiTags('medias')
export class MediaApiController {
  constructor(private readonly mediaApiService: MediaApiService) {}

  @Get()
  @UseGuards(JwtAuthGuard)
  @ApiBearerAuth()
  @ApiOperation({ summary: 'Get medias' })
  @ApiOkResponse({
    description: 'Media list response',
    type: MediasJsonResponse,
  })
  async getMedias(
    @Query() queryParams: MediasQuery,
    @Req() req: any,
  ): Promise<MediasJsonResponse> {
    const res = await this.mediaApiService.getMedias({
      page: queryParams.page,
      limit: queryParams.limit,
      authentication: req.authentication,
    });
    return {
      medias: res[0],
      total: res[1],
    };
  }

  @ApiOperation({ summary: 'Get media by id' })
  @ApiResponse({
    status: 200,
    description: 'The found record',
    type: MediaJsonResponse,
  })
  @UseGuards(JwtAuthGuard)
  @ApiBearerAuth()
  @Get(':id')
  async getMedia(
    @Param('id') id: string,
    @Req() req: any,
  ): Promise<MediaJsonResponse> {
    return {
      media: await this.mediaApiService.getMedia(id, req.authentication),
    };
  }

  @ApiOperation({ summary: '***Please use from media microservice***' })
  @Post()
  async createMedia(): Promise<void> {
    return;
  }
}
