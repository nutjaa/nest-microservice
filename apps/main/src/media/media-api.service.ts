import { Inject, Injectable, Logger } from '@nestjs/common';
import { ClientProxy } from '@nestjs/microservices';
import { lastValueFrom } from 'rxjs';
import { MEDIA_SERVICE } from '../constants/services';

@Injectable()
export class MediaApiService {
  private readonly logger = new Logger(MediaApiService.name);

  constructor(@Inject(MEDIA_SERVICE) private mediaClient: ClientProxy) {}

  async getMedia(id: string, authentication: string) {
    return await lastValueFrom(
      this.mediaClient.send('media_by_id', {
        id: id,
        authentication: authentication,
      }),
    );
  }

  async getMedias({
    page,
    limit,
    authentication,
  }: {
    page: number;
    limit: number;
    authentication: string;
  }) {
    return await lastValueFrom(
      this.mediaClient.send('medias', {
        page: page,
        limit: limit,
        authentication: authentication,
      }),
    );
  }
}
