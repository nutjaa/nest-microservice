import { Inject, Injectable, Logger } from '@nestjs/common';
import { ClientProxy } from '@nestjs/microservices';
import { AUTH_SERVICE, MEDIA_SERVICE } from './constants/services';
import { firstValueFrom } from 'rxjs';

@Injectable()
export class MainService {
  private readonly logger = new Logger(MainService.name);

  constructor(
    @Inject(MEDIA_SERVICE) private mediaProxy: ClientProxy,
    @Inject(AUTH_SERVICE) private authProxy: ClientProxy,
  ) {}

  getHello(): string {
    return 'Hello World! 002';
  }

  getHelloMedia(): Promise<string> {
    this.logger.log('media_test start');
    const source = this.mediaProxy.send<string>('media_test', {});
    return firstValueFrom(source);
  }

  getHelloAuth(): Promise<string> {
    this.logger.log('auth_test start');
    const source = this.authProxy.send<string>('auth_test', {});
    return firstValueFrom(source);
  }
}
