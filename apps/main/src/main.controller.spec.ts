import { Test, TestingModule } from '@nestjs/testing';
import { MainController } from './main.controller';
import { MainService } from './main.service';
import mongoose from 'mongoose';
import { ConfigModule } from '@nestjs/config';
import { AuthModule, DatabaseModule, RmqModule } from '@app/common';
import { MEDIA_SERVICE } from './constants/services';
import { AuthApiModule } from './auth/auth-api.module';
import { MediaApiModule } from './media/media-api.module';

describe('MainController', () => {
  let mainController: MainController;
  let app: TestingModule;

  beforeEach(async () => {
    app = await Test.createTestingModule({
      imports: [
        ConfigModule.forRoot({
          isGlobal: true,

          envFilePath: './apps/main/.env',
        }),
        DatabaseModule,
        RmqModule.register({
          name: MEDIA_SERVICE,
        }),
        AuthModule,
        AuthApiModule,
        MediaApiModule,
      ],
      controllers: [MainController],
      providers: [MainService],
    }).compile();

    mainController = app.get<MainController>(MainController);
  });

  afterAll(async () => {
    // Closing the DB connection allows Jest to exit successfully.
    await mongoose.connection.close();
    await app.close();
  });

  describe('root', () => {
    it('should return "Hello World!"', () => {
      expect(mainController.getHello()).toBe('Hello World! 002');
    });
  });
});
