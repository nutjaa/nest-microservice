import { Injectable, Logger } from '@nestjs/common';
import { InjectConnection, InjectModel } from '@nestjs/mongoose';
import { Model, Connection } from 'mongoose';
import { AbstractRepository } from '@app/common';
import { Media } from './schemas/media.schema';

@Injectable()
export class MediaRepository extends AbstractRepository<Media> {
  protected readonly logger = new Logger(MediaRepository.name);

  constructor(
    @InjectModel(Media.name) userModel: Model<Media>,
    @InjectConnection() connection: Connection,
  ) {
    super(userModel, connection);
  }
}
