import { Module } from '@nestjs/common';
import { MediaController } from './media.controller';
import { MediaService } from './media.service';
import { AuthModule, DatabaseModule, RmqModule } from '@app/common';
import { ConfigModule } from '@nestjs/config';
import { MongooseModule } from '@nestjs/mongoose';
import { Media, MediaSchema } from './schemas/media.schema';
import { MediaRepository } from './media.repository';

@Module({
  imports: [
    DatabaseModule,
    RmqModule,
    MongooseModule.forFeature([{ name: Media.name, schema: MediaSchema }]),
    ConfigModule.forRoot({
      isGlobal: true,
      envFilePath: './apps/media/.env',
    }),
    AuthModule,
  ],
  controllers: [MediaController],
  providers: [MediaService, MediaRepository],
})
export class MediaModule {}
