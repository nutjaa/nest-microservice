import { IsNumber } from 'class-validator';
import { Transform } from 'class-transformer';
import { ApiProperty } from '@nestjs/swagger';

export class UploadMediaQuery {
  @IsNumber()
  @Transform((value) => parseInt(value.value))
  @ApiProperty({
    example: '1',
    description: 'Media Type',
  })
  mediaType: number;
}
