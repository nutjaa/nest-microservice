import { Test, TestingModule } from '@nestjs/testing';
import { MediaController } from './media.controller';
import { MediaRepository } from './media.repository';
import { MediaService } from './media.service';
import { ConfigModule } from '@nestjs/config';
import { AuthModule, DatabaseModule, RmqModule } from '@app/common';
import { MongooseModule } from '@nestjs/mongoose';
import { Media, MediaSchema } from './schemas/media.schema';
import mongoose from 'mongoose';

describe('MediaController', () => {
  let mediaController: MediaController;
  let app: TestingModule;

  beforeEach(async () => {
    app = await Test.createTestingModule({
      imports: [
        DatabaseModule,
        RmqModule,
        MongooseModule.forFeature([{ name: Media.name, schema: MediaSchema }]),
        ConfigModule.forRoot({
          isGlobal: true,
          envFilePath: './apps/media/.env',
        }),
        AuthModule,
      ],
      controllers: [MediaController],
      providers: [MediaService, MediaRepository],
    }).compile();

    mediaController = app.get<MediaController>(MediaController);
  });

  afterAll(async () => {
    // Closing the DB connection allows Jest to exit successfully.
    await mongoose.connection.close();
    await app.close();
  });

  describe('root', () => {
    it('should return "Hello World!"', () => {
      expect('Hello World!').toBe('Hello World!');
    });
  });
});
