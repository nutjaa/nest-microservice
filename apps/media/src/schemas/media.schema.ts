import { Schema, SchemaFactory, Prop } from '@nestjs/mongoose';
import { AbstractDocument } from '@app/common';
import { SchemaTypes, Types } from 'mongoose';

export const MEDIA_TYPE_AVATAR = 1;

export const MEDIA_TYPE_LISTS = [MEDIA_TYPE_AVATAR];

const S3_BASE_URL = 'https://patrolman.s3.ap-southeast-1.amazonaws.com/';

@Schema({
  versionKey: false,
  timestamps: true,
  toJSON: { virtuals: true },
  toObject: { virtuals: true },
})
export class Media extends AbstractDocument {
  @Prop()
  mediaType: number;

  @Prop()
  key: string;

  @Prop()
  width?: number;

  @Prop()
  height?: number;

  @Prop({ required: true, type: SchemaTypes.ObjectId })
  userId: Types.ObjectId;

  @Prop({ type: Date })
  createdAt?: Date;

  @Prop({ type: Date })
  updatedAt?: Date;

  fullUrl?: string;
}

const MediaSchema = SchemaFactory.createForClass(Media);
MediaSchema.virtual('fullUrl').get(function (this: Media) {
  return `${S3_BASE_URL}${this.key}`;
});

export { MediaSchema };
