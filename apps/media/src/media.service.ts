import { Injectable, Logger } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import * as path from 'path';
import * as sharp from 'sharp';
import { S3 } from 'aws-sdk';
import { Media } from './schemas/media.schema';
import { v4 as uuid } from 'uuid';
import { MediaRepository } from './media.repository';
import { Types } from 'mongoose';

interface IS3Upload {
  dataBuffer: Buffer;
  key: string;
  width?: number;
  heigth?: number;
}

interface IUploadMedia {
  dataBuffer: Buffer;
  filename: string;
  mediaType: number;
  userId: string;
}

@Injectable()
export class MediaService {
  private readonly logger = new Logger(MediaService.name);

  constructor(
    private readonly configService: ConfigService,
    private readonly mediaRepository: MediaRepository,
  ) {}

  getHello(): string {
    this.logger.log('get hello');
    return 'Hello World from media!';
  }

  async UploadMedia({
    dataBuffer,
    filename,
    mediaType,
    userId,
  }: IUploadMedia): Promise<Media> {
    const newId = uuid();
    const newFileName = `${newId}${path.extname(filename)}`;
    const prefix = 'uploads/';
    const key = `${prefix}${newFileName}`;
    const s3configs: IS3Upload[] = [];

    // Add default
    s3configs.push({
      dataBuffer: dataBuffer,
      key: key,
    });

    await Promise.all(
      s3configs.map(async (s3config) => {
        await this.s3upload(s3config);
      }),
    );

    const media = await this.mediaRepository.create({
      key: key,
      mediaType: mediaType,
      userId: new Types.ObjectId(userId),
    });
    return media;
  }

  async s3upload({ dataBuffer, key, width, heigth }: IS3Upload): Promise<void> {
    const s3 = new S3();

    let data = dataBuffer;
    if (width != null && heigth != null) {
      data = await sharp(dataBuffer)
        .resize({ width: width, height: heigth, fit: sharp.fit.cover })
        .toBuffer();
    } else if (width != null) {
      data = await sharp(dataBuffer).resize({ width: width }).toBuffer();
    }

    await s3
      .upload({
        Bucket: this.configService.get('AWS_PUBLIC_BUCKET_NAME'),
        Body: data,
        Key: key,
        ACL: 'public-read',
      })
      .promise();
  }

  async getMedia(getMediaArgs: Partial<Media>) {
    return this.mediaRepository.findOne(getMediaArgs, false);
  }

  async getMedias(getMediaArgs: Partial<Media>, page: number, limit: number) {
    return this.mediaRepository.find(
      getMediaArgs,
      { page: page, limit: limit },
      false,
    );
  }
}
