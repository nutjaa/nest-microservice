import {
  Controller,
  Post,
  Query,
  Request,
  UploadedFile,
  UseGuards,
  UseInterceptors,
} from '@nestjs/common';
import { EventPattern, MessagePattern, Payload } from '@nestjs/microservices';
import { MediaService } from './media.service';
import { RmqService } from '@app/common';
import { JwtAuthGuard } from '@app/common/auth/jwt-auth.guard';
import {
  ApiBearerAuth,
  ApiConsumes,
  ApiOkResponse,
  ApiOperation,
} from '@nestjs/swagger';
import { ApiFile } from '@app/common/swagger/swagger-decorator';
import { FileInterceptor } from '@nestjs/platform-express';
import { UploadMediaQuery } from './query/upload-media.query';
import { MediaJsonResponse } from '@app/common/dto/mediaJson.response';
import { Types } from 'mongoose';
import { Media } from './schemas/media.schema';

@Controller('medias')
export class MediaController {
  constructor(
    private readonly mediaService: MediaService,
    private readonly rmqService: RmqService,
  ) {}

  @EventPattern('media_test')
  getMediaTest(/* @Payload() data: any, @Ctx() context: RmqContext */): string {
    return this.mediaService.getHello();
  }

  @UseGuards(JwtAuthGuard)
  @MessagePattern('media_by_id')
  async getMedia(@Payload() data: { id: string }): Promise<Media> {
    const res = await this.mediaService.getMedia({
      _id: new Types.ObjectId(data.id),
    });
    return res;
  }

  @UseGuards(JwtAuthGuard)
  @MessagePattern('medias')
  async getMedias(
    @Payload() data: { page: number; limit: number },
  ): Promise<[Media[], number]> {
    return await this.mediaService.getMedias({}, data.page, data.limit);
  }

  @UseGuards(JwtAuthGuard)
  @ApiOperation({ summary: 'Create new media' })
  @ApiBearerAuth()
  @ApiConsumes('multipart/form-data')
  @ApiFile('file')
  @UseInterceptors(FileInterceptor('file', {}))
  @Post()
  @ApiOkResponse({
    description: 'User response',
    type: MediaJsonResponse,
  })
  async createMedia(
    @UploadedFile() file: Express.Multer.File,
    @Query() query: UploadMediaQuery,
    @Request() req,
  ) {
    return {
      media: await this.mediaService.UploadMedia({
        dataBuffer: file.buffer,
        filename: file.originalname,
        mediaType: query.mediaType,
        userId: req.user.userId,
      }),
    };
  }
}
