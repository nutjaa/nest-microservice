import { Schema, SchemaFactory, Prop } from '@nestjs/mongoose';
import { AbstractDocument } from '@app/common';

@Schema({ versionKey: false, timestamps: true })
export class RefreshToken extends AbstractDocument {
  @Prop({ unique: true, type: String, required: true })
  refresh_token: string;

  @Prop()
  username: string;

  @Prop({ type: Date })
  valid: Date;

  @Prop({ type: Date })
  createdAt?: Date;

  @Prop({ type: Date })
  updatedAt?: Date;
}

export const RefreshTokenSchema = SchemaFactory.createForClass(RefreshToken);
