import { Injectable, Logger } from '@nestjs/common';
import { InjectConnection, InjectModel } from '@nestjs/mongoose';
import { Model, Connection } from 'mongoose';
import { AbstractRepository } from '@app/common';
import { RefreshToken } from './schemas/refresh-token.schema';

@Injectable()
export class RefreshTokensRepository extends AbstractRepository<RefreshToken> {
  protected readonly logger = new Logger(RefreshTokensRepository.name);

  constructor(
    @InjectModel(RefreshToken.name) refreshtokenModel: Model<RefreshToken>,
    @InjectConnection() connection: Connection,
  ) {
    super(refreshtokenModel, connection);
  }
}
