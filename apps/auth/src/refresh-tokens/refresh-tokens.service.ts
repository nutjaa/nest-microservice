import { Injectable, UnauthorizedException } from '@nestjs/common';
import { RefreshTokensRepository } from './refresh-tokens.repository';
import { RefreshToken } from './schemas/refresh-token.schema';
import { randomBytes } from 'crypto';

@Injectable()
export class RefreshTokensService {
  constructor(
    private readonly refreshTokensRepository: RefreshTokensRepository,
  ) {}

  async createRefreshToken(username: string): Promise<RefreshToken> {
    const date = new Date();
    const user = await this.refreshTokensRepository.create({
      username: username,
      refresh_token: randomBytes(64).toString('hex'),
      valid: new Date(date.setMonth(date.getMonth() + 8)),
    });
    return user;
  }

  async getRefreshToken(getRefreshTokenArgs: Partial<RefreshToken>) {
    return this.refreshTokensRepository.findOne(getRefreshTokenArgs);
  }

  async exchangeToken(refresh_token: string): Promise<RefreshToken> {
    const refreshToken = await this.getRefreshToken({
      refresh_token: refresh_token,
    });
    if (refreshToken == undefined || refreshToken.valid < new Date()) {
      throw new UnauthorizedException();
    }

    return refreshToken;
  }
}
