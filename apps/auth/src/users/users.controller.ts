import { Controller } from '@nestjs/common';
import { UsersService } from './users.service';
import { EventPattern, Payload } from '@nestjs/microservices';
import { CreateUserRequest } from '@app/common/dto/auth/create-user.request';
import { UserResponse } from '@app/common/dto/user.response';

@Controller()
export class UsersController {
  constructor(private readonly userService: UsersService) {}

  @EventPattern('user_create')
  async createUser(
    @Payload() data: { request: CreateUserRequest },
  ): Promise<UserResponse> {
    return new UserResponse(await this.userService.createUser(data.request));
  }
}
