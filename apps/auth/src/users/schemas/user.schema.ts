import { Schema, SchemaFactory, Prop } from '@nestjs/mongoose';
import { AbstractDocument } from '@app/common';
import { Exclude } from 'class-transformer';

//const S3_BASE_URL = 'https://patrolman.s3.ap-southeast-1.amazonaws.com/';

@Schema({ versionKey: false, timestamps: true })
export class User extends AbstractDocument {
  @Exclude()
  @Prop({ unique: true, type: String, required: true })
  username: string;

  @Prop()
  password: string;

  @Prop({ type: Date })
  createdAt?: Date;

  @Prop({ type: Date })
  updatedAt?: Date;
}

export const UserSchema = SchemaFactory.createForClass(User);
