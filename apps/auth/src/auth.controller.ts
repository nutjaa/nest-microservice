import { Controller, Logger, UseGuards } from '@nestjs/common';
import { AuthService } from './auth.service';
import { EventPattern, MessagePattern, Payload } from '@nestjs/microservices';
import { LoginUserRequest } from '@app/common/dto/auth/login-user.request';
import { UsersService } from './users/users.service';
import JwtAuthGuard from './guards/jwt-auth.guard';
import { CurrentUser } from './current-user.decorator';
import { User } from './users/schemas/user.schema';
import { UserResponse } from '@app/common/dto/user.response';
import { RefreshTokensService } from './refresh-tokens/refresh-tokens.service';
import { RefreshTokenRequest } from '@app/common/dto/auth/refresh-token.request';

@Controller()
export class AuthController {
  private readonly logger = new Logger(AuthController.name);

  constructor(
    private readonly authService: AuthService,
    private readonly usersService: UsersService,
    private readonly refreshTokensService: RefreshTokensService,
  ) {}

  @EventPattern('auth_test')
  getHello(): string {
    return this.authService.getHello();
  }

  @EventPattern('auth_login')
  async login(
    @Payload() data: { request: LoginUserRequest },
  ): Promise<[string, string]> {
    const user = await this.usersService.validateUser(
      data.request.username,
      data.request.password,
    );
    const refreshToken = await this.refreshTokensService.createRefreshToken(
      user.username,
    );
    return [this.authService.login(user), refreshToken.refresh_token];
  }

  @EventPattern('auth_refresh_token')
  async refreshToken(
    @Payload() data: { request: RefreshTokenRequest },
  ): Promise<[string, string]> {
    const refreshToken = await this.refreshTokensService.exchangeToken(
      data.request.refresh_token,
    );
    const user = await this.usersService.getUser({
      username: refreshToken.username,
    });
    return [this.authService.login(user), refreshToken.refresh_token];
  }

  @UseGuards(JwtAuthGuard)
  @MessagePattern('validate_user')
  async validateUser(@CurrentUser() user: User): Promise<UserResponse> {
    return new UserResponse(user);
  }
}
